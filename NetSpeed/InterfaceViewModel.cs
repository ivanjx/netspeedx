﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NetSpeed
{
    public class InterfaceViewModel
    {
        public NetworkInterface Interface { get; private set; }

        public IPAddress InterfaceAddress { get; private set; }

        public string InterfaceName { get; private set; }

        public string InterfaceIPStr { get; private set; }

        public IPAddress InterfaceIP { get; private set; }


        public InterfaceViewModel(NetworkInterface intf)
        {
            Interface = intf;
            InterfaceName = intf.Description;

            UnicastIPAddressInformationCollection unicol = intf.GetIPProperties().UnicastAddresses;
            foreach (UnicastIPAddressInformation info in unicol)
            {
                InterfaceAddress = info.Address;
                InterfaceIPStr = InterfaceAddress.ToString();
                break;
            }
        }
    }
}
