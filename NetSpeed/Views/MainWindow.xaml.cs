﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Drawing;
using System.Windows.Resources;

namespace NetSpeed
{
    public partial class MainWindow : MetroWindow, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        string m_upspeed;
        string m_downspeed;
        string m_upspeedUnit;
        string m_downspeedUnit;

        long m_receivedBytesOneSec;
        long m_sentBytesOneSec;
        
        List<InterfaceViewModel> m_interfaces;

        public string UpSpeed
        {
            get
            {
                return m_upspeed;
            }
            private set
            {
                m_upspeed = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("UpSpeed"));
            }
        }

        public string DownSpeed
        {
            get
            {
                return m_downspeed;
            }
            private set
            {
                m_downspeed = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DownSpeed"));
            }
        }

        public string UpSpeedUnit
        {
            get
            {
                return m_upspeedUnit;
            }
            private set
            {
                m_upspeedUnit = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("UpSpeedUnit"));
            }
        }

        public string DownSpeedUnit
        {
            get
            {
                return m_downspeedUnit;
            }
            private set
            {
                m_downspeedUnit = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DownSpeedUnit"));
            }
        }


        public MainWindow()
        {
            InitializeComponent();
            

            txtUpSpeed.DataContext = this;
            txtUpSpeedUnit.DataContext = this;

            txtDownSpeed.DataContext = this;
            txtDownSpeedUnit.DataContext = this;

            m_interfaces = new List<InterfaceViewModel>();

            NetworkChange.NetworkAddressChanged += (object sender, EventArgs e) => 
            {
                UpdateInterfaces();
            };
            NetworkChange.NetworkAvailabilityChanged += (object sender, NetworkAvailabilityEventArgs e) => 
            {
                UpdateInterfaces();
            };

            UpdateInterfaces();
            UpdateLabels();
        }

        async void UpdateInterfaces()
        {
            NetworkInterface[] availableInterfaces = (from t in NetworkInterface.GetAllNetworkInterfaces()
                                                      where t.OperationalStatus == OperationalStatus.Up && t.NetworkInterfaceType != NetworkInterfaceType.Loopback
                                                      select t).ToArray();

            m_interfaces.Clear();

            for (int i = 0; i < availableInterfaces.Length; ++i)
            {
                m_interfaces.Add(new InterfaceViewModel(availableInterfaces[i]));
                await Task.Delay(10);
            }
        }

        async void UpdateLabels()
        {
            (float size, string unit) SizeUnit(float size)
            {
                float finalSize = 0;
                string unitName = string.Empty;

                if (size < 1024)
                {
                    unitName = "B/s";
                    finalSize = size;
                }
                else if (size < 1024 * 1024)
                {
                    unitName = "KB/s";
                    finalSize = size / 1024;
                }
                else if (size < 1024 * 1024 * 1024)
                {
                    unitName = "MB/s";
                    finalSize = size / (1024 * 1024);
                }
                else
                {
                    unitName = "GB/s";
                    finalSize = size / (1024 * 1024 * 1024);
                }

                return (size: finalSize, unit: unitName);
            }

            while (m_interfaces.Count == 0)
            {
                await Task.Delay(100);
            }

            try
            {
                long totalSentDiff = 0;
                long totalReceivedDiff = 0;

                for (int i = 0; i < m_interfaces.Count; ++i)
                {
                    IPInterfaceStatistics intstat = m_interfaces[i].Interface.GetIPStatistics();

                    totalSentDiff += intstat.BytesSent - m_sentBytesOneSec;
                    totalReceivedDiff += intstat.BytesReceived - m_receivedBytesOneSec;

                    m_sentBytesOneSec = intstat.BytesSent;
                    m_receivedBytesOneSec = intstat.BytesReceived;
                }

                (float size, string unit) sent, received;
                sent = SizeUnit(totalSentDiff);
                received = SizeUnit(totalReceivedDiff);

                UpSpeed = sent.size.ToString("F1");
                UpSpeedUnit = sent.unit;

                DownSpeed = received.size.ToString("F1");
                DownSpeedUnit = received.unit;
            }
            catch
            {
            }
            finally
            {
                await Task.Delay(1000);
                UpdateLabels();
            }
        }
    }
}
