﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;

namespace NetSpeed
{
    public partial class SmallWindow : MetroWindow
    {
        NotifyIcon m_trayIcon;
        MainWindow m_mainwindow;


        public SmallWindow()
        {
            void PlaceLowerRight()
            {
                //Determine "rightmost" screen
                Screen rightmost = Screen.AllScreens[0];
                foreach (Screen screen in Screen.AllScreens)
                {
                    if (screen.WorkingArea.Right > rightmost.WorkingArea.Right)
                        rightmost = screen;
                }

                this.Left = rightmost.WorkingArea.Right - this.Width - 10;
                this.Top = rightmost.WorkingArea.Bottom - this.Height - 10;
            }

            void InitTrayIcon()
            {
                m_trayIcon = new NotifyIcon();
                m_trayIcon.Text = "NetSpeedX";

                StreamResourceInfo streamInfo = System.Windows.Application.GetResourceStream(new Uri("Resources/32x32.ico", UriKind.Relative));
                using (streamInfo.Stream)
                {
                    m_trayIcon.Icon = new Icon(streamInfo.Stream);
                }

                m_trayIcon.DoubleClick += (object sender, EventArgs e) =>
                {
                    if (m_mainwindow == null)
                    {
                        m_mainwindow = new MainWindow();
                        m_mainwindow.Show();
                    }
                };

                m_trayIcon.Visible = true;
            }

            InitializeComponent();
            PlaceLowerRight();
            InitTrayIcon();
        }
    }
}
